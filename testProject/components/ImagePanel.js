import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, Button,Easing, Animated} from 'react-native';
import {Dimensions} from 'react-native';

class ImagePanel extends Component {
  constructor(props) {
    super(props);
    // initialize state of animation
    this.spinValue = new Animated.Value(0)
    this.spinning = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg','0deg']
    })
    //
    this.state = {
      currentAngle : 0
    }
  }
  
  rotating = () =>{
    this.setState( (state,props) => ({
      currentAngle: state.currentAngle+props.angleSteer
    }))
    this.spinning =  this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.state.currentAngle+'deg',(this.state.currentAngle+90)+'deg']
    })
  }

  componentDidMount = () => { 
    this.spinValue = new Animated.Value(0);
    Animated.timing(this.spinValue,{
      toValue: 1,
      duration: 3000,
      easing: Easing.linear
    }).start(()=>{ 
      this.componentDidMount()
    });

  }

  render() {
    return (
      <View style={style.wrapper}>
        <Animated.Image
          source={this.props.pic}
          style={[style.img, {transform: [{rotate: this.spinning}]}]}
        />
        <Text style={style.text}>{this.props.textContent}</Text>
        <Button
          title="Press me"
          onPress={this.rotating}
        />
      </View>
    );
  }
};

 const style = StyleSheet.create({
      wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      text: {
        color: '#FFDD00',
        fontSize: 20,
        fontWeight: 'bold',
        textDecorationLine: 'underline',
      },
      img: {
        width: Math.round((Dimensions.get('window').width * 2) / 3),
        height: Math.round((Dimensions.get('window').height * 2) / 3),
      },
    });

ImagePanel.defaultProps = {
    pic : {
      uri: 'https://www.randomuser.me/api/portraits/men/1.jpg',
    },
    textContent : "Hello react native",
    angleSteer : 90
};



export default ImagePanel;