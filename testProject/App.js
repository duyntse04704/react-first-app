import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native'
import {Dimensions} from 'react-native';
import ImagePanel from './components/ImagePanel'
const App = () => {
  return (
    <View style={style.container}>
      <ImagePanel/>
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default App;